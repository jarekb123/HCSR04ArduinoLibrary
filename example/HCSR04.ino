/*
 Name:		HCSR04.ino
 Created:	01.05.2017 22:59:52
 Author:	Jaroslaw Butajlo
 Editor:	http://www.visualmicro.com
*/

#include <HCSR04Lib.h>

#define TRIGGER 9
#define ECHO 8
#define MEASURE_CYCLE 500 // mesasure cycle (from documentation) - 60ms

#define BUTTON 3

bool isMeasurmentShow = false;

HCSR04 * distanceSensor = new HCSR04(TRIGGER, 400);

// the setup function runs once when you press reset or power the board
void setup() {
	Serial.begin(115200);
	attachInterrupt(digitalPinToInterrupt(BUTTON), buttonOnClick, RISING);
	distanceSensor->initTimer();
}


unsigned long startTime = 0;
// the loop function runs over and over again until power down or reset
void loop() {
	if (!isMeasurmentShow && distanceSensor->isMeasurmentReady()) {
		Serial.println(distanceSensor->getLatestMeasure());
		isMeasurmentShow = true;
	}
}

void buttonOnClick() {
	Serial.println(__FUNCTION__);
	distanceSensor->startMeasure();
	isMeasurmentShow = false;
}