/*
Name:		HCSR04Lib.cpp
Created:	02.05.2017 11:59:52
Author:	Jaroslaw Butajlo

*/

#include "HCSR04Lib.h"



// Timer1Callback - see header file for details

Timer1Callback * mTimer1Callback = nullptr;

void setTimer1Callback(Timer1Callback * timer1Callback) {
	mTimer1Callback = timer1Callback;
}

HCSR04::HCSR04(int trigPin, int maxDistance) {
	this->trigPin = trigPin;
	this->echoPin = icpPin; // Input Capture Pin - Timer1
	this->maxDistance = maxDistance;
	this->mIsMeasurmentReady = false;

	pinMode(trigPin, OUTPUT);
	pinMode(echoPin, INPUT);
	
	setTimer1Callback(this);
}

void HCSR04::initTimer() {
	cli(); // disable interrupts
	TCCR1A = 0; // Timer1 Normal Mode
	TCCR1B = 0; // reset Control B register

	TCNT1 = 0; // reset Timer1 Counter Register

	//	TCCR1B |= (_BV(WGM13)); // phase and freq corrected mode
	/* After reseting Control Register Timer works in Normal Mode - can generate Overflow interrputs */
	TIMSK1 = 0; // reset Timer1 Interrupt Mask Register (TIMSK) - for safety

	/*
		TOIE1 - Timer1 Overflow Interrupt Enable
		ICIE1 - Input Capture Interrupt Enable (Timer1)
	*/

	// setting max to Output Compare Mode
	OCR1A = (long)maxDistance * 58L * F_CPU / 256L / 1000000L;

	TIMSK1 |= (_BV(TOIE1) | _BV(OCIE1A) | _BV(ICIE1)); // Interupts Enabling
	TCCR1B |= (_BV(ICES1)); // Rising Edge of icpPin detecting
	TIFR1 |= (_BV(ICF1)); // clear interrupt flags
	TCCR1B |= (_BV(WGM12) | _BV(CS12)); // prescaling - clock / 256 - dimension 3mm
	sei(); // enable interrupts
}

void HCSR04::startMeasure() {
	mIsMeasurmentReady = false;
	if (!isMeasureOn) {
		isMeasureOn = true;

		digitalWrite(trigPin, HIGH);
		delayMicroseconds(10); // 10us TTL signal (width from documentation)
		digitalWrite(trigPin, LOW);
	}
}

void HCSR04::risingEdge() {
	TCNT1 = 0; // reset counter
	timerOvfCounter = 0;
	TCCR1B &= ~(_BV(ICES1)); // falling edge next

}
void HCSR04::fallingEdge() {
	this->capturedICR = ICR1;
	TCCR1B |= _BV(ICES1);
	isMeasureOn = false;
	mIsMeasurmentReady = true;
}
float HCSR04::getLatestMeasure() {
	this->pulseWidth = this->capturedICR + timerOvfCounter * TIMER1MAX;
	return ((float)this->pulseWidth * 1000000.0 / ((float)F_CPU / 256.0)) / 58.0;
}
void HCSR04::reachedMaxDistance() {
	TCCR1B |= _BV(ICES1);
	isMeasureOn = false;

	this->capturedICR = maxDistance * 58 * F_CPU / 256 / 1000000;

	
}
bool HCSR04::isMeasuring() {
	return isMeasureOn;
}

bool HCSR04::isMeasurmentReady()
{
	return mIsMeasurmentReady;
}

void HCSR04::isrOvf() {
	timerOvfCounter++;
}

void HCSR04::isrCapt() {
	if (TCCR1B & (_BV(ICES1))) { // detected rising edge
								 //	Serial.println("rising");
		this->risingEdge();

	}
	else {
		//	Serial.println("falling");
		this->fallingEdge();

	}
}
void HCSR04::isrCompa() {
	if (this->isMeasuring()) {
		//	Serial.println("maxdistance");
		this->reachedMaxDistance();
		//distanceSensor->fallingEdge();
	}
}

ISR(TIMER1_OVF_vect) {
	if (mTimer1Callback) mTimer1Callback->isrOvf();
	
}

ISR(TIMER1_CAPT_vect) {
	if (mTimer1Callback) mTimer1Callback->isrCapt();
}

ISR(TIMER1_COMPA_vect) {
	if (mTimer1Callback) mTimer1Callback->isrCompa();
}