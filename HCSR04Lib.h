/*
 Name:		HCSR04Lib.h
 Created:	01.05.2017 22:59:52
 Author:	Jaroslaw Butajlo
 Editor:	http://www.visualmicro.com
*/

#ifndef _HCSR04Lib_h
#define _HCSR04Lib_h

#define TIMER1MAX 65535 // max value of Timer1 16-bit register 
#define icpPin 8
#include <Arduino.h>


// Timer1 Callback Class
// This class is used to call methods from HCSR04 object, which are non-static methods.
// Only one sensor is able to control the timer1 registers.
class Timer1Callback {
public:
	virtual void isrOvf();
	virtual void isrCompa();
	virtual void isrCapt();
};


void setTimer1Callback(Timer1Callback * timer1Callback);

class HCSR04 : Timer1Callback{
public:
	// echoPin must be only PIN_2 or PIN_3 in Arduino Uno, because edge of echo signal runs interrupts
	HCSR04(int trigPin, int maxDistance);
	// only trigPin can be set, echoPin is an Input Capture Pin of Timer1 - PIN_8
	HCSR04(int trigPin);
	// Starts measuring
	void startMeasure();
	// Return last measured distance in centimeters (cm)
	float getLatestMeasure();
	// Check if new measured value is available
	bool isMeasuring();

	bool isMeasurmentReady();

	// Initialize the Timer1 registers
	void initTimer();

	// ======= Timer1Callback methods override ======== /
	void isrCapt();
	void isrCompa();
	void isrOvf();


private:
	void risingEdge();
	void fallingEdge();
	void reachedMaxDistance();

	int trigPin, echoPin;
	unsigned long capturedICR; // latest measured Input Capture Register (in cycles)
	unsigned long pulseWidth;
	int timerOvfCounter; // timer1 overflow counter
	bool isMeasureOn; // true if Timer1 is counting
	bool mIsMeasurmentReady; // true if measurment is finished
	int maxDistance;

};

#endif

